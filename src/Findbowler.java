import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class Findbowler {
    Set<String> ecobowl= new HashSet<>();
    public Set<String>EconomicalBowler(Map<String,Float> map, Float value){
        if(map.containsValue(value)){
            for(Map.Entry<String,Float> entry : map.entrySet()){
                if(Objects.equals(entry.getValue(),value)){
                    ecobowl.add(entry.getKey());
                }
            }
        }
        return ecobowl;
    }

}

