import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Deliveries {
    // Problem 3
    Map<String,Integer> team_extra= new HashMap<>();
    public void read() {
        try{
            String line="";
            String line2="";
            String spiltby=",";
            BufferedReader br= new BufferedReader((new FileReader("matches.csv")));
            br.readLine();
            while((line=br.readLine()) != null){
                String[] mat1= line.split(spiltby);
                if(mat1[1].equals("2016")){

                    BufferedReader br1= new BufferedReader((new FileReader("deliveries.csv")));
                    br1.readLine();
                    while((line2=br1.readLine()) != null){
                        String[] mat2= line2.split(spiltby);
                        if(mat2[0].equals(mat1[0])){

                            if(! team_extra.containsKey(mat2[2])){
                                team_extra.put(mat2[2],0);
                            }
                            else{
                                team_extra.put(mat2[2], team_extra.get(mat2[2])+Integer.parseInt(mat2[16]));
                            }
                        }
                    }
                }
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println('\n'+"PROBLEM 3: SOLUTION");
        System.out.println(team_extra);
    }
}
