import java.util.Map;

public class Findmin {

    public float min(Map<String,Float> m){
        float min= Float.MAX_VALUE;
        for(Map.Entry<String,Float> entry : m.entrySet()){
            if(entry.getValue() < min){
                min= entry.getValue();
            }
        }
        return min;
    }
}
