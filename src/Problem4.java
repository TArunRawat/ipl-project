import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Problem4 {
    // Problem 3
    Float count= 1F,a=0F;
    Map<String,Float> bowler_runs= new HashMap<>();
    Map<String,Float> bowler_balls= new HashMap<>();
    Set<String> result= new HashSet<>();

    public void read() {
        try{
            Findmin fm = new Findmin();
            Findbowler fb= new Findbowler();
            String line="";
            String line2="";
            String spiltby=",";
            BufferedReader br= new BufferedReader((new FileReader("matches.csv")));
            br.readLine();
            while((line=br.readLine()) != null){
                String[] mat1= line.split(spiltby);
                if(mat1[1].equals("2015")){

                    BufferedReader br1= new BufferedReader((new FileReader("deliveries.csv")));
                    br1.readLine();
                    while((line2=br1.readLine()) != null){
                        String[] mat2= line2.split(spiltby);
                        if(mat2[0].equals(mat1[0])){
                            if(! bowler_runs.containsKey(mat2[8])){
                                bowler_runs.put(mat2[8], 0F);
                            }
                            else{
                                bowler_runs.put(mat2[8], bowler_runs.get(mat2[8])+Float.parseFloat(mat2[17]));
                            }

                            if(! bowler_balls.containsKey(mat2[8])){
                                bowler_balls.put(mat2[8],1F);
                                count=1F;
                            }
                            else{
                                count++;
                                bowler_balls.put(mat2[8], count);
                            }
                        }
                    }
                }
            }
            bowler_runs.forEach((key,value)->bowler_balls.merge(key,value,(v1,v2)-> (v2/v1)*6));
            a= fm.min(bowler_balls);
            result=fb.EconomicalBowler(bowler_balls,a);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println('\n'+"PROBLEM 4: SOLUTION");
        System.out.println("Economical Bowler in IPL in 2015 :"+" "+result);
    }

}
