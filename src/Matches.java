import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Matches {
    // Transferring matches.csv into data Structure. Will be using ArrayList as a data Structure.
    //public static List<Match> match_data= new ArrayList<>();
    List<String> ls= new ArrayList<>();
    List<String> ls2= new ArrayList<>();
    List<String> ls1= new ArrayList<>();
    public void read() {
        String filepath = "matches.csv";
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";
        String[] mat= new String[0];
        try {
            br = new BufferedReader(new FileReader(filepath));
            while ((line = br.readLine()) != null) {
                mat = line.split(splitBy);
                ls.add(mat[1]);
                ls2.add(mat[10]);
                ls1.add(mat[3]);
            }

        } catch (Exception e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ip) {
                    ip.printStackTrace();
                }
            }
        }
        // PROBLEM 1:
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String s : ls){
            map.put(s,Collections.frequency(ls,s));
        }
        System.out.println("Problem1 Solution:");
        System.out.println(map);

        //PROBLEM 2:
       // System.out.println(ls2);
        Map<String, Integer> map1 = new HashMap<String, Integer>();
        for (String s : ls2){
            map1.put(s,Collections.frequency(ls2,s));
        }
        map1.remove("");
        System.out.println('\n'+"Problem2 Solution:");
        System.out.println(map1);

        // PROBLEM 5: CUSTOM (Which team has won most of the matches in IPL)
        int max= Integer.MIN_VALUE;
        String team=null;
        Set<Map.Entry<String,Integer>> entries=map1.entrySet();
        for(Map.Entry<String,Integer> entry:entries) {
            if(entry.getValue()>max) {
                max=entry.getValue();
                team=entry.getKey();
            }
        }

        System.out.println('\n'+"Problem5 Solution");
        System.out.println(team+" : "+max);
    }
}
